## Add PlotlyJson extractor for Line Plots

A new extractor "Plotly Json" is now available. It can be used to extract
data+style of line plots in a json file following the plotly-json
[schema](https://plotly.com/chart-studio-help/json-chart-schema/).
This allows to export line plots generated in catalyst scripts to any library
that accepts plotly-json files.
