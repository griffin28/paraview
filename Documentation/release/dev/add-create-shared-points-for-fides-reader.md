## CreateSharedPoints Property For FidesReader

You can now request the FidesReader to create shared points
at the interface between partitions of uniform grids. This option
closes the gaps that used to appear when visualizing the output datasets.
