## Add Macro Dialog

A new dialog has been added to edit your macros. It replaces the old submenus
from the Macros menu with a new dialog that allows you to add, edit or remove
macros.

![Edit Macros Dialog](add-macro-dialog.png)
